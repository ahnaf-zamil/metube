
#include <signal.h>
#include <unistd.h>

#include <iostream>

#include "AMQPcpp.h"
#include "dotenv.h"
#include "nlohmann/json.hpp"
#include "worker.hpp"

#define QUEUE_NAME "upload_process"
#define ENV_FILE "./.env"

using json = nlohmann::json;

void signal_handler(int signum)
{
    std::cout << "Stopping" << std::endl;
    exit(0);
}

void check_and_load_dotenv()
{
    // Checking if dotenv exists, loading if it does
    if (access(ENV_FILE, F_OK) == 0)
    {
        std::cout << "Loading dotenv" << std::endl;
        dotenv::init(ENV_FILE);
    }
}

std::string get_broker_url()
{
    std::string AMQP_USER = std::getenv("AMQP_USER");
    std::string AMQP_PASS = std::getenv("AMQP_PASS");
    std::string AMQP_HOST = std::getenv("AMQP_HOST");

    return AMQP_USER + ":" + AMQP_PASS + "@" + AMQP_HOST;
}

int onMessage(AMQPMessage *message)
{
    uint32_t j = 0;
    char *data = message->getMessage(&j);

    if (data)
    {
        json payload = json::parse(data);
        std::string upload_id = payload["upload_id"].get<std::string>();

        const char *cmsg_p = upload_id.c_str();
        char *msg_p = strdup(cmsg_p);
        // Creating thread to start encoding
        pthread_t worker_thread;
        pthread_create(&worker_thread, NULL, Worker::init_thread, (void *)msg_p);
    }
}
int main()
{
    check_and_load_dotenv();
    std::string BROKER_URI = get_broker_url();

    signal(SIGINT, signal_handler);
    signal(SIGQUIT, signal_handler);

    try
    {
        AMQP amqp(BROKER_URI);
        AMQPQueue *queue = amqp.createQueue(QUEUE_NAME);
        queue->Declare(QUEUE_NAME, AMQP_DURABLE);

        queue->addEvent(AMQP_MESSAGE, onMessage);
        std::cout << "Consumer started" << std::endl;
        queue->Consume(AMQP_NOACK);
    }
    catch (AMQPException e)
    {
        printf("%s\n", e.getMessage().c_str());
    }

    return 0;
}
