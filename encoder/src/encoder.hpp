#ifndef ENCODER_H
#define ENCODER_H

#include <iostream>
#include <sys/stat.h>

const int res_list[] = {1080, 720, 480, 360, 240};

namespace Encoder
{
std::string cmd_template = "ffmpeg -hide_banner -loglevel warning -i \"%s\" -vf \"scale=-2:%d%s\" -crf 24 "
                           "%s";
std::string height_cmd_template = "ffmpeg -i \"%s\" 2>&1 | grep Video: | grep -Po '\\d{3,5}x\\d{3,5}' | cut "
                                  "-d'x' -f2";

void encode_res(char *exec_cmd)
{
    FILE *fp;
    char path[1035];

    fp = popen(exec_cmd, "r");
    if (fp == NULL)
    {
        std::cerr << "Failed to run comand\n";
        return;
    }

    while (fgets(path, sizeof(path), fp) != NULL)
    {
        std::cout << path << std::endl;
    }

    pclose(fp);
    return;
}

int get_video_height(const char *file_name)
{
    // Building command
    int exec_len = height_cmd_template.length() + strlen(file_name) + 1;
    char *exec = (char *)malloc(exec_len * sizeof(char));
    std::snprintf(exec, exec_len, height_cmd_template.c_str(), file_name);

    char buf[10];
    char *str = NULL;
    char *temp = NULL;
    unsigned int size = 1;
    unsigned int strlength;

    FILE *out;
    if (NULL == (out = popen(exec, "r")))
    {
        perror("popen");
        return -1;
    }

    while (fgets(buf, sizeof(buf), out) != NULL)
    {
        strlength = strlen(buf);
        temp = (char *)realloc(str, size + strlength);
        if (temp == NULL)
        {
            perror("realloc");
            return -1;
        }
        else
        {
            str = temp;
        }

        strcpy(str + size - 1, buf);
        size += strlength;
    }

    return atoi(buf);
}

void start_encode(std::string upid, std::string fname)
{
    std::cout << "(" + upid + ") Started encoder" << std::endl;

    const char *upload_id = upid.c_str();
    const char *file_name = fname.c_str();

    int src_height = get_video_height(file_name);

    mkdir(upload_id, 0777);

    for (int i = 0; i < sizeof(res_list) / sizeof(int); i++)
    {
        // Going over every resolution list
        int selected_res = res_list[i];

        if (selected_res <= src_height)
        {
            std::string output_name(upload_id);
            output_name += "/" + std::to_string(selected_res) + ".mp4";

            std::string fps_flag_len = "";
            if (selected_res < 720)
            {
                fps_flag_len = ",fps=fps=30";
            }
            // Building FFMPEG command
            int buff_len = cmd_template.length() + strlen(file_name) + std::to_string(selected_res).length() +
                           fps_flag_len.length() + output_name.length() + 1;
            char *buff = (char *)malloc(buff_len * sizeof(char));
            std::snprintf(buff, buff_len, cmd_template.c_str(), file_name, selected_res, fps_flag_len.c_str(),
                          output_name.c_str());

            // Start encoding with command
            std::cout << "(" + upid + ") Encoding to " << selected_res << "p resolution" << std::endl;
            encode_res(buff);
            free(buff);
        }
    }
    std::cout << "(" + upid + ") Finished encoding" << std::endl;
}
} // namespace Encoder

#endif
